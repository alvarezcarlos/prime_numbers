FROM mhart/alpine-node:12.18.0

WORKDIR /primes

COPY package*.json ./

RUN npm install

COPY /src/ ./src

RUN npm test

CMD [ "npm", "start" ]
const { it, expect } = require("@jest/globals");
const { validateInput, isPrime } = require("../utils/utils");

describe("Utils Tests", () => {
    it("module to be defined", () => {
        expect(validateInput).toBeDefined;
    })
    it("module to be defined", () => {
        expect(isPrime).toBeDefined;
    })
    it("is prime, not prime number", () => {
        expect(isPrime(4)).toBeFalsy;
    })
    it("is prime, prime number ", () => {
        expect(isPrime(5)).toBeTruthy;
    })
    it("validate input, invalid number ", () => {
        expect(validateInput(-1)).toBeFalsy;
    })
    it("validate input, valid number ", () => {
        expect(validateInput(5)).toBeFalsy;
    })
})
const { validateInput, isPrime } = require("./utils/utils.js");

const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Type a number: ", function (num) {
  if (!validateInput(Number(num))) rl.close();

  let primes = [];

  for (let j = Number(num); j > 0; j--) {
    if (isPrime(j)) {
      primes.push(j);
    }
  }

  console.log(primes.join(","));
  rl.close();
});

rl.on("close", function () {
  console.log("\nBYE BYE !!!");
  process.exit(0);
});
const validateInput = (num) => {
    if (!/^[1-9]\d*$/.test(num)) {
      console.log("number has to be positive integer");
      return false
    }
    return true
  };
  
  const isPrime = (num) => {
    for (let i = 2; i < num; i++) if (num % i === 0) return false;
    return num >= 1;
  };
  
  module.exports = { validateInput, isPrime };